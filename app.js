'use strict';
const pg = require('pg');

module.exports = app => {

  app.beforeStart(function* () {
    app.logger.debug('the config is ' + app.config.name);
    app.pool_yushan = new pg.Pool(app.config.yushan);
  });
  app.on('request', ctx => {
    ctx.logger.info(ctx.request.url);
  });

  app.yushanQuery = function(sql, param) {
    return new Promise(function(resolve, reject) {
      app.pool_yushan.connect(function(err, client, done) {
        if (err) {
          return console.error('error fetching client from pool', err);
        }
        client.query(sql, param, function(error, results) {
          done();
          if (error) {
            console.log(sql);
            console.log(param);
            return reject(error);
          }
          resolve(results.rows);
        });
      });
    });
  };
};
