'use strict';

module.exports = appInfo => {
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1528861046604_158';

  exports.view = {
    defaultViewEngine: 'nunjucks',
  };
  exports.nunjucks = {
    // dir: 'path/to/template/dir',  // default to `{app_root}/app/view`
    cache: true, // local env is false
  };
  exports.multipart = {
    autoFields: false,
    defaultCharset: 'utf8',
    fieldNameSize: 100,
    fieldSize: '100kb',
    fields: 10,
    fileSize: '100mb',
    files: 10,
    fileExtensions: [],
    whitelist: [
      // tar
      '.zip','.rar',
      '.gz', '.tgz', '.gzip'
    ],
  };
  defaultViewEngine:'nunjucks',
  // add your config here
  config.middleware = ['bejson'];
  config.yushan = {
    user: 'rdspg',
    database: 'yushan',
    password: 'anmeng',
    host: 'rds455ekt1422z8sh7e2o.pg.rds.aliyuncs.com',
    port: '3432',
    max: 10,
    idleTimeoutMillis: 30000,
  };
  config.security = {
    csrf: {
      enable: false,
    },
    methodnoallow: {
          enable: false
        }, 
        domainWhiteList: [ '192.168.1.112:5500' ]
  };
  
  return config;
};
