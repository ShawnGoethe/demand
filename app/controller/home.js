'use strict';

module.exports = app =>{
   class HomeController extends app.Controller{
    async render() {
      await this.ctx.render('index.html');
    }
    async tableView() {
      await this.ctx.render('table.html');
    }
   }
   return HomeController;
};
