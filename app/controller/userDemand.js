'use strict';

var fs = require('fs');
const path = require('path');
const sendMail = require("./mail");
module.exports = app=>{
  class DemandController extends app.Controller{
    async show() {
      await this.ctx.render('table.html');
    }
    async view(){
      const {search,sort,interval,status,page,num} = this.ctx.request.query;//sort.name="sort column"  sort.value=1 asc   sort.value=-1 desc
      let sql = "select * from tb_user_demand where 1=1  ";
      let sizeSql= "select count(*) FROM tb_user_demand where 1=1";

      if(search){
        sql += " and (account like '%"+search+"%' or domain like '%"+search+"%' )" ;
        sizeSql += " and (account like '%"+search+"%' or domain like '%"+search+"%' )";
      }
      if(status){
        sql += " and status="+status+" ";
        sizeSql +=" and status="+status+" ";
      }
      if(interval){
        const interval2Json = JSON.parse(interval);
        sql += " and (sub_date >= '"+interval2Json.start+"' and sub_date <= '"+interval2Json.end+"' )";
        sizeSql +=" and(sub_date >= '"+interval2Json.start+"' and sub_date <= '"+interval2Json.end+"' )";
      }
      if(sort){//sort demand by column name
        const sort2Json = JSON.parse(sort);
        if(sort2Json.value==1){
          sort2Json.valueName='asc';
        }
        else{
          sort2Json.valueName = 'desc';
        }
         sql += ' order by '+sort2Json.name+' '+sort2Json.valueName;
      }else{
        sql+=" order by sub_date desc";
      }
      
      sql+=" limit "+num+" offset "+(page-1)*num;
      console.log(sql);
      console.log(sizeSql);
      let results = await app.yushanQuery(sql,[]);
      let ret = {}
      ret['data'] = results

      const demandSize = await app.yushanQuery(sizeSql,[]);
      ret["count"]=demandSize[0].count;
      this.ctx.body = ret;
      
    }
    async new(){
      const { account,domain , zip_link,cnt } = this.ctx.request.query;
      const sql ="insert into tb_user_demand (account,user_name,domain,link,status,zip_link,sub_date,cnt) values($1,'',$2,'',0,$3,now(),$4)";
      const ret = await app.yushanQuery(sql,[account,domain,zip_link,cnt]);
      ret.status=true;
      sendMail('2881486189@qq.com','我又有新的需求了', 'Hi 田野,这是一封新需求提醒邮件');
      this.ctx.body = JSON.stringify(ret.status);
    }
    async modify(){
      const { cnt } = this.ctx.request.query;
      const sql ="insert into tb_user_demand (cnt,status,sub_date) values($1,0,now())";
      const ret = await app.yushanQuery(sql,[cnt]);
      ret.status=true;
      sendMail('2881486189@qq.com','我又有新的修改需求了', 'Hi 田野,这是一封新的修改需求提醒邮件');
      this.ctx.body = JSON.stringify(ret.status);
    }
    async delete(){
      const { id } = this.ctx.request.query;
      const sql ="update tb_user_demand set status=2 where id=$1";
      const ret = await app.yushanQuery(sql,[id]);

      const result = await app.yushanQuery("select zip_link from tb_user_demand where id=$1",[id]);
      const target = path.join(this.config.baseDir, 'app/public', result[0].zip_link)
      fs.unlink(target, function (err) {
        if (err) return console.log(err);
        console.log('文件删除成功');
      })

      ret.status=true;
      this.ctx.body = JSON.stringify(ret.status);
    }
    async complete(){
      const {link, id } = this.ctx.request.query;
      const linkString = JSON.parse(link);
      const sql ="update tb_user_demand set status=1,link=$1 where id=$2";
      const ret = await app.yushanQuery(sql,[linkString,id]);
      ret.status=true;

      const result = await app.yushanQuery("select zip_link from tb_user_demand where id=$1",[id]);
      if(result[0].zip_link!=null){
        const target = path.join(this.config.baseDir, 'app/public', result[0].zip_link)
        fs.unlink(target, function (err) {
        if (err) return console.log(err);
        console.log('文件删除成功');
        })
      }
      this.ctx.body = JSON.stringify(ret.status);
    }
  }
  return DemandController;
}
