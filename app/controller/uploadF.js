'use strict';

const fs = require('fs');
const path = require('path');
const Controller = require('egg').Controller;
const awaitWriteStream = require('await-stream-ready').write;
const sendToWormhole = require('stream-wormhole');

class UploadMultipleController extends Controller {
  async show() {
    await this.ctx.render('page/multiple.html');
  }

  async upload() {
    const parts = this.ctx.multipart({ autoFields: true });
    const files = [];

    let stream;
    while ((stream = await parts()) != null) {
      var name = stream.filename.toLowerCase();
      const arr = name.split(".");
      let filename=new Date().getTime();
      for(let i=0;i<arr.length-1;i++){
        filename += arr[i]+'.';
      } 
      filename += arr[arr.length-1];//新文件名

      //const filename = stream.filename.toLowerCase();
      const target = path.join(this.config.baseDir, 'app/public', filename);
      console.log(filename+"  "+target);
      const writeStream = fs.createWriteStream(target);
      try {
        await awaitWriteStream(stream.pipe(writeStream));
      } catch (err) {
        await sendToWormhole(stream);
        throw err;
      }
      files.push(filename);
    }

    this.ctx.body = files;
    // await this.ctx.render('page/multiple_result.html', {
    //   files,
    // });
  }
}

module.exports = UploadMultipleController;
