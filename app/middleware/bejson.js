
'use strict';
const AssertionError = require('assert').AssertionError;
module.exports = () => {
  return function* bejson(next) {
    try {
      yield next;
      if (Buffer.isBuffer(this.body)) return;//如果是文件退出
      if (/^\/\*\*\//.test(this.body)) return;
      if (this.status === 404) return;
      this.body = {
        code: 0,
        status: 'success',
        data: this.body,
      };
    } catch (error) {
      this.body = {
        code: 1,
        status: 'failed',
        message: error.message,
      };
      console.log(error);
    }
  };
};
