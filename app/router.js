'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/demand/view', controller.userDemand.view);
  router.get('/demand/new', controller.userDemand.new);
  router.get('/demand/modify', controller.userDemand.modify);
  router.get('/demand/delete', controller.userDemand.delete);
  router.get('/demand/complete', controller.userDemand.complete);

  router.redirect('/', '/public/qudao/index.html', 302);

  router.get('/demand/uploadF', app.controller.uploadF.show);
  router.post('/demand/uploadF', app.controller.uploadF.upload);


};
